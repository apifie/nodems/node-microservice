> Read it our [detailed Project Wiki](https://gitlab.com/apifie/nodems/node-microservice/wikis/how-to-contribute)

# Contributing
Contributions are welcome!

## Before you get started

### Aggrement
Before you can contribute, you must agree to Apache License 2.0

### Code of conduct

As contributors and maintainers of this project, and in the interest of fostering an open and welcoming community, we pledge to respect all people who contribute through reporting issues, posting feature requests, updating documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free experience for everyone, regardless of level of experience, gender, gender identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, religion, or nationality.

Examples of unacceptable behavior by participants include:

- The use of sexualized language or imagery
- Personal attacks
- Trolling or insulting/derogatory comments
- Public or private harassment
- Publishing other's private information, such as physical or electronic addresses, without explicit permission
- Other unethical or unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct. By adopting this Code of Conduct, project maintainers commit themselves to fairly and consistently applying these principles to every aspect of managing this project. Project maintainers who do not follow or enforce the Code of Conduct may be permanently removed from the project team.

This code of conduct applies both within project spaces and in public spaces when an individual is representing the project or its community.

## Finding something to work on
Help is always welcome! For example, documentation (like the text you are reading now) can always use improvement. There's always code that can be clarified and variables or functions that can be renamed or commented. There's always a need for more test coverage. You get the idea - if you ever see something you think should be fixed, you should own it. Here is how you can get started

### Project issue list
Have a look at existing open project issues. You may find something interesting to work there which other people from community want are facing problems with.

### Reviewing merge requests
You can also help in reviewing the merge request, testing them and providing feedback.

### File an Issue
Not ready to contribute code, but see something that needs work? While the community encourages everyone to contribute code, it is also appreciated when someone reports an issue.

## Contributing

Many contributors and maintainers work on it in their personal time. We request you to have little patience in getting your work merged. It might take them some time to respond.

### Open a pull request
We generally follow github workflow.

### Code review
There are two aspects of code review: giving and receiving.

To make it easier for your PR to receive reviews, consider the reviewers will need you to:

- follow the project coding conventions
- write good commit messages
- break large changes into a logical series of smaller patches which individually make easily understandable changes, and in aggregate solve a broader issue

Reviewers, the people giving the review, are highly encouraged to revisit the Code of Conduct and must go above and beyond to promote a collaborative, respectful community.
When reviewing PRs from others [The Gentle Art of Patch Review](http://sage.thesharps.us/2014/09/01/the-gentle-art-of-patch-review/) suggests an iterative series of focuses which is designed to lead new contributors to positive collaboration without inundating them initially with nuances:

- Is the idea behind the contribution sound?
- Is the contribution architected correctly?
- Is the contribution polished?

### Testing

Testing is the responsibility of all contributors. There are multiple types of tests.
- Unit: These confirm that a particular function behaves as intended.
- Integration: These tests cover interactions of components or interactions between components and some other resource
- End-to-end ("e2e"): These are broad tests of overall system behavior and coherence.

### Security

If the contribution is related to fixing a security bug that if known to an adversary can cause harm to community already using a particular release, it is strongly advised to report in private with project maintainers and submitting the fix privately.

### Documentation
If you would like to help contribute to the documentation, we’re would be very happy. You can
- improve existing docs
- improve docs accessibility
- write a blog post
- document new features
- create diagrams
- graphics assets, and embeddable screencasts / videos

## Merge request template

<!--  Thanks for sending a pull request!  Here are some tips for you:
1. If you want *faster* PR reviews, read https://github.com/kubernetes/community/blob/master/contributors/guide/pull-requests.md#best-practices-for-faster-reviews
2. Make sure tests are added and passing for the new code.
-->

**What this PR does / why we need it**:


**Which issue this PR fixes**: 
<!-- (example: fixes #123) -->


**What changes did you make? (Give an overview)**:  


**Special notes for your reviewer**:


**If applicable**:
- [ ] documentation
- [ ] unit tests
- [ ] tested backward compatibility (ie. deploy with previous version, upgrade with this branch)

**Release note**:
<!--  Steps to write your release note:
Enter your extended release note in the below block; leaving it blank means using the PR title as the release note. If no release note is required, just write `NONE`.
-->
```
```