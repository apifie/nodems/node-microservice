const cls = require('cls-hooked')

const namespace = cls.createNamespace('default')

const { serviceBootstrapper, logger } = require('../../../index')
const { getExpressApp } = require('./create-app')
const chai = require('chai')

let server
let app
logger.silly('namespace ', namespace.get('default'))

function setChai() {
  logger.info('Setting Chai keys')
  chai.config.proxyExcludedKeys.push('eventually')
  chai.config.proxyExcludedKeys.push('rejected')
}

async function startServer() {
  logger.info('Starting test app')
  const res = getExpressApp()
  logger.debug('Inside start server ', res)
  server = res.server
  return res.app
}

async function stopServer() {
  logger.info('Stopping test app')
  app = undefined
  if (server) {
    logger.info('Shutting down test server')
    try {
      await server.close(() => {
        logger.info('Test server is shut down now')
      })
    } catch (err) {
      console.log('error in shutting down the test server %s', err)
    }
  }
}

async function startApp(serviceSetup, mqListeners) {
  try {
    const apifiedApp = await serviceBootstrapper(serviceSetup, mqListeners, app)
    return apifiedApp
  } catch (err) {
    console.log('Some error in starting test app %j ', err)
    throw err
  }
}

before(async () => {
  console.log('In before test')
  setChai()
  app = await startServer()
  console.log('Got test express app in before ')
})

after(async () => {
  logger.info('In after test')
  await stopServer()
})

module.exports = {
  startApp
}
