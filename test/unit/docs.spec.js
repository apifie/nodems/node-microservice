const { startApp } = require('./helpers/test-server-setup')
const {
  getUtils,
} = require('../../index')
const fs = require('fs')

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
 
// Register the plugin
chai.use(chaiAsPromised)

const expect = chai.expect

const { serviceSetup, mqListeners } = require('../config/service-setup')

describe('When we try to bootstrap an apifie service, with route enabled config setup', () => {    
    
    after(async () => {
        const _setup = JSON.parse(JSON.stringify(serviceSetup))
        console.log("Deleting the generated docs file");
        fs.unlink(_setup.apiDocsPath.postmanSpecPath+"internal-api-collection.json",(error)=>{
            if (error) {throw error;}
        });
        fs.unlink(_setup.apiDocsPath.postmanSpecPath+"api-collection.json",(error)=>{
            if (error) {throw error;}            
        });
        fs.unlink(_setup.apiDocsPath.swaggerSpecPath+"internal-swagger-spec.json",(error)=>{
            if (error) {throw error;}            
        });
        fs.unlink(_setup.apiDocsPath.swaggerSpecPath+"swagger-spec.json",(error)=>{
            if (error) {throw error;}            
        });
    })

    it('should be able to generate spec', async () => {
        const _setup = JSON.parse(JSON.stringify(serviceSetup))
        const apifiedApp = await startApp(_setup, mqListeners)
        expect(apifiedApp, 'App is not undefined').not.to.be.an('undefined')
        expect(getUtils, 'Utils is not undefined').to.not.throw()
        expect(getUtils().generateApiDocs(_setup.apiDocsPath)).to.be.true;
    })
})

