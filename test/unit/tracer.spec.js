const { startApp } = require('./helpers/test-server-setup')
const { logger } = require('../../index')
const { getTracerClient } = require('../../index')

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
const expect = chai.expect

const { serviceSetup, mqListeners } = require('../config/service-setup')

describe('When we apifie a service with tracer enabled and correct config', () => {
  before(async () => {
    await startApp(serviceSetup, mqListeners)
  })

  it('we get a tracer and a logger ', async () => {
    expect(getTracerClient(), 'tracer is not undefined').not.to.be.an('undefined')
    expect(logger, 'logger is not undefined').not.to.be.an('undefined')
  })

  it('and loger has all required logging functions ', () => {
    expect(logger.silly, 'Logger can log silly').to.be.a('function')
    expect(logger.info, 'Logger can log info').to.be.a('function')
    expect(logger.warn, 'Logger can log warn').to.be.a('function')
    expect(logger.error, 'Logger can log error').to.be.a('function')
  })

  xit('and looger and tracer are linked on transaction Id ', () => {
    // TODO : Find a way to test this
  })
})
