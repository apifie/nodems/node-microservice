const { startApp } = require('./helpers/test-server-setup')
const { getSqlDBClient } = require('../../index')

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
const expect = chai.expect

const { serviceSetup, mqListeners } = require('../config/service-setup')

describe('When we apifie a service with db enabled and correct config', () => {
  before(async () => {
    await startApp(serviceSetup, mqListeners)
  })

  it('we get a db client ', async () => {
    expect(getSqlDBClient, 'db is not undefined').to.not.throw()
  })

  it('and it has models ', () => {
    expect(getSqlDBClient().TestModel, 'db has a TestModel').not.to.be.an('undefined')
    expect(getSqlDBClient().TestChild, 'db has a TestModel').not.to.be.an('undefined')
  })

  xit('and it has associations ', () => {
    // TODO : Check / fix associations code in db.js and find a way to test it here
  })
})
