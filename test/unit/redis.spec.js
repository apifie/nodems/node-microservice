const { startApp } = require('./helpers/test-server-setup')
const { getCacheClient } = require('../../index')

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
const expect = chai.expect

const { serviceSetup, mqListeners } = require('../config/service-setup')

describe('When we apifie a service with cache enabled and correct config', () => {
  it('we get a cache client that is functional', async () => {
    await startApp(serviceSetup, mqListeners)
    expect(getCacheClient, 'cache is not undefined').to.not.throw()
    expect(getCacheClient().setRedisKey, 'has a function to set key-value').to.be.a('function')
    expect(getCacheClient().getRedisKey, 'has a function to get a value').to.be.a('function')
    expect(getCacheClient().deleteRedisKey, 'has a function to delete a key-value').to.be.a('function')
  })

  describe('when we set a key-value in redis', () => {
    before(async () => {
      await getCacheClient().setRedisKey('k1', 'v1')
    })

    after(async () => {
      await getCacheClient().deleteRedisKey('k1')
    })

    it('We can get a key-value that exist', async () => expect(getCacheClient().getRedisKey('k1')).to.eventually.equal('v1'))

    it('We can not get a key-value that does not exist', async () => expect(getCacheClient().getRedisKey('xx')).to.eventually.to.become(null))

    it('We can delete a key', async () => expect(getCacheClient().deleteRedisKey('k1')).to.eventually.not.throw)

    it('We can not get a key-value after deletion', async () => expect(getCacheClient().getRedisKey('k1')).to.eventually.to.become(null))
  })
})
