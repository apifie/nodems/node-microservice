const { startApp } = require('./helpers/test-server-setup')
const { getRandomDataSuccess, getRandomDataErr } = require('./helpers/api-helper')
const { serviceSetup, mqListeners } = require('../config/service-setup')
const { getApiClient } = require('../../index')

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const uuid = require('uuid')

chai.use(chaiAsPromised)
const expect = chai.expect
const cls = require('cls-hooked')

const namespace = cls.getNamespace('default')
describe('When we apifie a service with api enabled', () => {
  before(async () => {
    await startApp(serviceSetup, mqListeners)
  })

  it('we get a api client that is functional', async () => {
    expect(getApiClient, 'api is not undefined').to.not.throw()
    const api = getApiClient()
    expect(api, 'api is a function that makes axios request').to.be.a('function')
  })

  it('we receive 404 when we call invalid routes', async () =>
    namespace.runAndReturn(async () => {
      const tid = uuid.v4()
      namespace.set('transactionid', tid)
      // transactionid are needed as header in api call, throws error if header goes undefined
      try {
        await getApiClient()(getRandomDataErr())
        throw new Error('Should not have resolved')
      } catch (err) {
        expect(err.status, 'status should be 404').to.be.equal(404)
      }
    }))

  it('we receive 200 when we call valid routes', async () => {
    await namespace.runAndReturn(async () => {
      const tid = uuid.v4()
      namespace.set('transactionid', tid)
      // transactionid are needed as header in api call, throws error if header goes undefined
      const res = await getApiClient()(getRandomDataSuccess())
      expect(res.status, 'status should be 200').to.be.equal(200)
    })
  })
})
