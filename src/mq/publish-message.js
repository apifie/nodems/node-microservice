// method to publish a message, will queue messages internally if the connection is down and resend later
const cls = require('cls-hooked')
const logger = require('../trace/logger')
const { config } = require('../setup')
const ComponentError = require('../errors/component-error')

const namespace = cls.getNamespace('default')
let pubChannel = null

function setPubChannel(ch) {
  pubChannel = ch
}

function publishMessage(exchange, routingKey, event, metadata, data, msgType, delay, retryCount) {
  return new Promise((resolve, reject) => {
    if (!pubChannel) {
      logger.error('[AMQP] publish channel not available')
      reject(new ComponentError('[AMQP] publish channel not available'))
    }
    const headers = {
      'x-transactionid': namespace.get('transactionid'),
      msgType
    }
    if (msgType === 'REQUEST_RETRY') {
      headers['x-delay'] = delay || (10 * 1000)
      headers.retryCount = retryCount
    }

    // TODO: Do not assert an exchange here. Propertis may conflict. Find a ronbust way of denying this publish-request
    /* if (msgType !== 'REQUEST_RETRY') {
     await pubChannel.assertExchange(toExchange, 'topic', {durable: true})
     } else {
     await pubChannel.assertExchange(toExchange, 'x-delayed-message', {
     durable: true,
     arguments: {'x-delayed-type': 'topic'}
     })
     } */

    logger.info('Publishing message. Exchange = %s, RoutingKey = %s, Event = %s, msgType = %s, delay = %s', exchange, routingKey, event, msgType, delay)
    const ms = {
      event,
      metadata,
      data
    }

    pubChannel.publish(exchange, routingKey, Buffer.from(JSON.stringify(ms)), {
      persistent: true,
      headers
    }, (err) => {
      if (err) {
        logger.error('[AMQP] Error in publishing message %j', err)
        pubChannel.connection.close()
        return reject(err)
      }
      const result = {
        exchange,
        routingKey,
        event: ms.event,
        metadata: ms.metadata
      }
      return resolve(result)
    })
  })
}

async function sendRequest(exchange, routingKey, event, metadata, data) {
  const result = await publishMessage(exchange, routingKey, event, metadata, data, 'REQUEST')
  return result
}

async function sendResponse(exchange, routingKey, event, metadata, data, isTargeted) {
  let finalExchange = exchange
  if (exchange.endsWith('-delayed')) {
    finalExchange = exchange.replace('-delayed', '')
  }
  const rkey = isTargeted ? routingKey : `${routingKey}-res`
  const result = await publishMessage(finalExchange, rkey, event, metadata, data, 'RESPONSE')
  return result
}

async function sendRetryRequest(msg, retryCount, delay) {
  if (!config.isRetryEnabled) {
    throw new ComponentError('Retry is not supported by service config')
  }
  const jmsg = JSON.parse(msg.content.toString())
  let publishExchangeName = msg.fields.exchange
  if (!msg.fields.exchange.endsWith('-delayed')) {
    publishExchangeName = `${msg.fields.exchange}-delayed`
  }
  const result = await publishMessage(publishExchangeName, msg.fields.routingKey, jmsg.event, jmsg.metadata, jmsg.data, 'REQUEST_RETRY', delay, retryCount)
  return result
}

async function sendIncident(msg, message, retryCount, error) {
  if (!config.isIncidentEnabled) {
    throw new ComponentError('Incident alerts are not supported by service config')
  }
  logger.debug('Got an incident for sending')
  const jmsg = {
    data: msg.content.toString(),
    metadata: {
      issue: {
        message,
        retryCount,
        error
      },
      target: {
        exchange: msg.fields.exchange,
        routingKey: msg.fields.routingKey,
        event: 'unknown'
      }
    }
  }

  try {
    const jsonMsg = JSON.parse(msg.content.toString())
    jmsg.metadata.target.event = jsonMsg.event
  } catch (err) {
    logger.debug('Okay to ignore this error ', err)
  }

  const routingKey = error && error.name ? error.name : 'alert'
  logger.debug('Prepared to send Incident ', jmsg.metadata, jmsg.data)
  const result = await publishMessage(config.incidentExchange, routingKey, 'alert', jmsg.metadata, jmsg.data, 'INCIDENT')
  return result
}

module.exports = {
  sendRequest,
  sendResponse,
  sendRetryRequest,
  sendIncident,
  setPubChannel,
  publishMessage
}
