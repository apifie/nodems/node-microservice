/*
 * This file process message received by consumer channel
 */

const Joi = require('joi')
const cls = require('cls-hooked')

const namespace = cls.getNamespace('default')
const logger = require('../trace/logger')
const publisher = require('./publish-message')
const SoftError = require('../errors/soft-error')
const HardError = require('../errors/hard-error')
const FailureIncident = require('../errors/failure-Incident')
const UnknownEvent = require('../errors/unknown-event-error')
const InvalidMessageStructure = require('../errors/invalid-message-structure-error')
const InvalidMessageContent = require('../errors/invalid-message-content-error')
const { config } = require('../setup')

const { getListener } = require('./mq-resources')

const messageSchema = Joi.object().keys({
  event: Joi.string().min(1).required(),
  data: Joi.alternatives([Joi.object(), Joi.array()]).required(),
  metadata: Joi.object().required()
})

function getReturnAddress(jmsg, msg) {
  const hasReturnAddress = jmsg.metadata.replyToExchange && jmsg.metadata.replyToRoutingKey
  return {
    replyToExchange: hasReturnAddress ? jmsg.metadata.replyToExchange : msg.fields.exchange,
    replyToRoutingKey: hasReturnAddress ? jmsg.metadata.replyToRoutingKey : msg.fields.routingKey,
    replyAsEvent: hasReturnAddress ? jmsg.metadata.replyAsEvent : jmsg.event,
    isTargeted: hasReturnAddress
  }
}

async function processMessage(msg) {
  logger.debug('***** Message in processor %j', msg)
  let jmsg

  if (!msg.properties || !msg.properties.headers || !msg.properties.headers.msgType) {
    logger.error('Message is missing properties or headers')
    throw new InvalidMessageStructure('Message is missing properties or headers')
  }

  const msgType = msg.properties.headers.msgType
  const retryCount = msg.properties.headers.retryCount || 0

  try {
    logger.debug('Mesage content = ', msg.content)
    jmsg = JSON.parse(msg.content.toString())
    logger.info('Received a message to process. Exchnage = %s, Routing key = %s, Event = %s, msgType = %s, retryCount = %d', msg.fields.exchange, msg.fields.routingKey, jmsg.event, msgType, retryCount)
  } catch (e) {
    logger.debug('Cannot parse payload on msg_queue\nError: %s\nPayload: %s', e.toString(), msg.content.toString())
    logger.error('Invalid json structure in message content %s', e.toString())
    throw new InvalidMessageContent('Invalid json structure in message content')
  }

  const validationResult = Joi.validate(jmsg, messageSchema, { abortEarly: false })
  if (validationResult.error) {
    logger.error('Message not according to spec\n%s\n%s', validationResult.error.message, JSON.stringify(jmsg, null, 2))
    throw new InvalidMessageStructure(`Message does not comply with apifie schema [${validationResult.error.message}]`)
  }

  const returnAddress = getReturnAddress(jmsg, msg)
  return namespace.runAndReturn(async () => {
    // TODO: Generate transaction id if not already available
    namespace.set('transactionid', msg.properties.headers['x-transactionid'])
    try {
      const result = await messageRouter(jmsg, retryCount)

      if (msgType === 'REQUEST' || msgType === 'REQUEST_RETRY') {
        logger.info('Sending back success response for %s ', jmsg.event)
        return await publisher.sendResponse(returnAddress.replyToExchange, returnAddress.replyToRoutingKey, returnAddress.replyAsEvent, jmsg.metadata, result, returnAddress.isTargeted)
      }
    } catch (err) {
      if (err instanceof InvalidMessageStructure) {
        logger.error('Rejecting Invalid message = %s', jmsg.event)
        throw err
      }
      if (err instanceof UnknownEvent) {
        logger.error('Rejecting unknown event = %s', jmsg.event)
        throw err
      } else if (err instanceof SoftError) {
        if (config.isRetryEnabled) {
          logger.warn('Got a soft error while processing %s. Retry after %dms', jmsg.event, err.delay)
          logger.info('Sending retry message to self with a delay of %d seconds', err.delay)
          const retryResult = await publisher.sendRetryRequest(msg, retryCount + 1, err.delay)
          return retryResult
        }
        logger.error('Got a soft error while processing %s. Retry after %dms', jmsg.event, err.delay)
        throw err
      } else if (err instanceof HardError) {
        logger.error('Got hard error while processing %s', jmsg.event)
        throw err
      } else if (err instanceof FailureIncident) {
        logger.error('Got FailureIncident while processing %s after %d attempts. Publishing an Incident', jmsg.event, retryCount)
        throw err
      } else {
        logger.error('Rejecting due to some unknown error = %s, %j', jmsg.event, err)
        throw err
      }
    }
  })
}

async function messageRouter(jmsg, retryCount) {
  const subscriber = getListener(jmsg.event)
  if (!subscriber) {
    throw new UnknownEvent(`No subscriber found. Unknown event type: ${jmsg.event}`)
  }

  // validate message format specification
  const metadataValidationResult = Joi.validate(jmsg, subscriber.specification, { abortEarly: false })
  if (metadataValidationResult.error) {
    logger.debug('Message not according to spec\n%s', metadataValidationResult.error.message)
    throw new InvalidMessageStructure(metadataValidationResult.error.message)
  }

  /*
   * message is according to specification, send the payload to the listener
   * and return the response from it to the caller
   */
  return subscriber.listener(jmsg.metadata, jmsg.data, retryCount)
}

module.exports = {
  processMessage
}
