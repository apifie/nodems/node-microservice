const express = require('express')
const swagger = require('swagger-spec-express')

const router = express.Router()
swagger.swaggerize(router)
const { config } = require('../setup')

/* send all env vars values used by services. Confidential env vars are named as X_{name}  */
router.get('/', (req, res) => {
  const envVars = {}
  // TODO : Fix for internal keys
  Object.keys(config.serviceConfig).map((key) => {
    if (!key.startsWith('x-')) {
      envVars[key] = config.serviceConfig[key]
    }
    return true
  })
  res.status(200).send(envVars)
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'run newman test cases'
    }
  }
})

module.exports = router
