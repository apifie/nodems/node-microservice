const express = require('express')
const swagger = require('swagger-spec-express')

const router = express.Router()
swagger.swaggerize(router)

const { getMQStatus } = require('../mq/mq-resources')
const { getRedisStatus } = require('../cache/redis')
const { config } = require('../setup')
const { getDBConnection } = require('../sqldb/db')

async function getDBStatus() {
  const db = getDBConnection()
  try {
    await db.sequelize.query('select * from SequelizeMeta')
    return {
      isActive: true
    }
  } catch (err) {
    return {
      isActive: false
    }
  }
}

router.get('/heartbeat', async (req, res) => {
  const dbStatus = await getDBStatus()
  const status = {
    sqldb: config.isDBEnabled ? dbStatus.isActive : undefined,
    mq: config.isMQEnabled ? getMQStatus().isActive : undefined,
    redis: config.isCacheEnabled ? getRedisStatus().isActive : undefined
  }
  res.status(200).send(status)
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'run newman test cases'
    }
  }
})

router.get('/stats', (req, res) => {
  res.status(200).send('stats')
}).describe({
  tags: ['internal'],
  responses: {
    200: {
      description: 'run newman test cases'
    }
  }
})

module.exports = router
