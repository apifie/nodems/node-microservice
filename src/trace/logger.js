const cls = require('cls-hooked')
const winston = require('winston')
require('winston-logstash')
const fs = require('fs')
const path = require('path')

let logger

function setLogger() {
  const logLevel = process.env.LOG_LEVEL || 'info'
  /* Logs of equal or higher priority than set log levels get printed.
   * These are valid log level values and priority. 0 is highest.
   * error: 0
   * warn: 1
   * info: 2
   * verbose: 3
   * debug: 4
   * silly: 5
   */
  logger = new winston.Logger()
  // https:// github.com/jaakkos/winston-logstash/blob/master/docs/configuration.md

  const data = JSON.parse(fs.readFileSync(`${path.join(process.cwd(), 'package.json')}`), 'utf8')
  if (process.env.NODE_ENV !== 'test') {
    logger.add(winston.transports.Logstash, {
      host: process.env.LOGSTASH_HOST,
      max_connect_retries: -1, // -1 means retry forever.
      node_name: data.name,
      port: process.env.LOGSTASH_PORT,
      level: logLevel
    })
  }

  logger.add(winston.transports.Console, {
    colorize: true,
    level: logLevel
  })

  logger.filters.push((level, msg) => {
    let tid = cls.getNamespace('default') ? cls.getNamespace('default').get('transactionid') : ''
    tid = tid ? `${tid} ` : ''
    return tid + msg
  })

  // patch all log levels because winston is not cls-* complaint, so that they keep track of continuation-local-storage
  // const logLevels = Object.keys(winston.config.npm.levels)

  logger.stream.write = (message) => {
    logger.info(message)
  }
}

setLogger()

module.exports = logger
