const InvalidConfig = require('./errors/invalid-config')

const config = {}
const setupServiceConfig = (serviceSetup, mqListeners) => {
  config.env = serviceSetup.env || 'production'
  config.microserviceName = `${serviceSetup.microserviceName}-${serviceSetup.microserviceVersion}`

  config.isTraceEnabled = serviceSetup.trace.enabled
  config.zipkinHost = serviceSetup.trace.zipkinHost
  config.zipkinPort = serviceSetup.trace.zipkinPort

  config.isCacheEnabled = serviceSetup.cache.enabled
  config.redisKey = serviceSetup.cache.redisKey
  config.redisPort = serviceSetup.cache.redisPort
  config.redisHost = serviceSetup.cache.redisHost
  config.redisKeyPrefix = serviceSetup.cache.redisKeyPrefix
  config.redisKeyTimeout = serviceSetup.cache.redisKeyTimeout || 24 * 60 * 60

  config.isDBEnabled = serviceSetup.db.enabled
  config.dbPool = serviceSetup.db.pool || {
    max: 10,
    min: 0,
    idle: 10000,
    acquire: 30000
  }

  config.isApiEnabled = serviceSetup.api.enabled
  config.API_TIMEOUT = serviceSetup.api.API_TIMEOUT || 60 * 1000

  config.isMQEnabled = serviceSetup.mq.enabled
  config.isRetryEnabled = serviceSetup.mq.enableDelayedRetry
  config.isIncidentEnabled = serviceSetup.mq.enableIncidentAlerts
  config.rabbitmqUser = serviceSetup.mq.rabbitmqUser
  config.rabbitmqPwd = serviceSetup.mq.rabbitmqPwd
  config.rabbitmqHost = serviceSetup.mq.rabbitmqHost
  config.rabbitmqPort = serviceSetup.mq.rabbitmqPort
  config.messageQueueHeartbeat = serviceSetup.mq.messageQueueHeartbeat || 60
  config.selfQueueName = `${serviceSetup.microserviceName}-${serviceSetup.microserviceVersion}`
  config.exchangesToSubscribe = serviceSetup.mq.exchangesToSubscribe
  config.concurrencyLimitPerWorker = Number.isInteger(serviceSetup.mq.concurrencyLimitPerWorker) ? serviceSetup.mq.concurrencyLimitPerWorker : 0
  config.enforceConsecutiveFlow = false
  if (serviceSetup.mq.enforceConsecutiveFlow) {
    if (serviceSetup.db.enabled) {
      config.enforceConsecutiveFlow = true
      config.concurrencyLimitPerWorker = 1
    } else {
      throw new InvalidConfig('DB must be enabled to enforceConsecutiveFlow')
    }
  }

  config.incidentExchangeName = 'incident'
  config.incidentExchangeVersion = '1.0.0'
  config.incidentExchange = `${config.incidentExchangeName}-${config.incidentExchangeVersion}`

  config.runIntegrationTests = serviceSetup.runIntegrationTests

  config.mqListeners = mqListeners
  config.serviceConfig = serviceSetup.serviceConfig
}

module.exports = {
  config,
  setupServiceConfig
}
