const axios = require('axios')
const wrapAxios = require('zipkin-instrumentation-axios')
const { getTracer } = require('../trace/tracer')
const { config } = require('../setup')
// Wrap an instance of axios

const createRequest = (apiConfig) => {
  const {
    url
  } = apiConfig

  const tracer = getTracer()
  const zipkinAxios = wrapAxios(axios, { tracer, serviceName: config.microserviceName })
  return zipkinAxios.sendRequest(url, apiConfig)
}

module.exports = createRequest
