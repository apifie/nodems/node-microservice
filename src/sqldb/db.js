const path = require('path')
const fs = require('fs')
const url = require('url')
const InvalidConfig = require('../errors/invalid-config')
const Sequelize = require('sequelize')
const Semaphore = require('mysql-semaphore')

const basename = path.basename(__filename)
const logger = require('./../trace/logger')

const db = {}
let mySqlConfig

function getDBConfigObject(dbUrl) {
  const config = {}
  const urlParts = url.parse(dbUrl)
  logger.debug('URL parts = ', urlParts)

  config.host = urlParts.hostname
  if (urlParts.pathname) {
    config.database = urlParts.pathname.replace(/^\//, '')
  }
  if (urlParts.port) {
    config.port = urlParts.port
  }
  if (urlParts.auth) {
    const authParts = urlParts.auth.split(':')
    config.user = authParts[0]
    if (authParts.length > 1) { config.password = authParts.slice(1).join(':') }
  }
  logger.debug('DB config = ', config)
  return config
}

async function initializeDb(withSemaphore, config, modelsPath) {
  logger.info('Initializing DB')
  if (config && modelsPath && config.use_env_variable) {
    const sequelize = await new Sequelize(process.env[config.use_env_variable], config)

    // TODO : Do this based on a config flag => runMigrationsOnStart
    /* sequelize.sync().then(() => {
      logger.info('DB connection sucessful')
    }, (err) => {
      // catch error here
      logger.error('Error while connecting to DB %j', err)
    }) */

    await fs.readdirSync(modelsPath).filter(file => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')).forEach((file) => {
      const model = sequelize.import(path.join(`${modelsPath}`, file))
      db[model.name] = model
      logger.info('Initialized Model : %s', model.name)
    })

    // TODO : Check this part. This code is never geting executed
    await Object.keys(db).forEach((modelName) => {
      if (db[modelName].associate) {
        db[modelName].associate(db)
      }
    })

    logger.debug('Need Semaphore %s ', withSemaphore)
    if (withSemaphore) {
      mySqlConfig = getDBConfigObject(process.env[config.use_env_variable])
      if (config.ssl) {
        mySqlConfig.ssl = config.ssl
      }
    }

    db.sequelize = sequelize
    db.Sequelize = Sequelize
    return db
  }
  logger.error('Cannot find DB connection config')
  throw new InvalidConfig('Cannot find DB connection config')
}

function getDBConnection() {
  return db
}

function getSemaphore() {
  logger.debug('Semaphore DB config = %j', mySqlConfig)
  const semaphore = new Semaphore(mySqlConfig)
  return semaphore
}

module.exports = {
  initializeDb,
  getDBConnection,
  getSemaphore
}
