const { BatchRecorder, ConsoleRecorder } = require('zipkin')
const { HttpLogger } = require('zipkin-transport-http')
const { config } = require('../setup')
const logger = require('../trace/logger')

function setUpZipkinRecorder() {
  let recorder = new ConsoleRecorder()
  if (config.isTraceEnabled) {
    logger.info('Setting HTTP request Recorder for tracer')
    recorder = new BatchRecorder({
      logger: new HttpLogger({
        endpoint: `http://${config.zipkinHost}:${config.zipkinPort}/api/v1/spans`
      })
    })
  }
  return recorder
}

module.exports = setUpZipkinRecorder
