const cls = require('cls-hooked')
const fs = require('fs')
const appLogger = require('morgan')
const EventEmitter = require('events')
const logger = require('./src/trace/logger')
const ComponentError = require('./src/errors/component-error')
const FailureIncident = require('./src/errors/failure-Incident')
const HardError = require('./src/errors/hard-error')
const InvalidConfig = require('./src/errors/invalid-config')
const InvalidMessageStructureError = require('./src/errors/invalid-message-structure-error')
const InvalidMessageContentError = require('./src/errors/invalid-message-content-error')
const SoftError = require('./src/errors/soft-error')
const UnknownEventError = require('./src/errors/unknown-event-error')
const Swagger2Postman = require('swagger2-to-postman')
const expressSanitizer = require('express-sanitizer')

const packageJson = require(`${process.cwd()}/package.json`)
const eventBus = new EventEmitter()

const bpState = {
  tracer: undefined,
  sqlDbClient: undefined,
  mqClient: undefined,
  cacheClient: undefined,
  routesMiddleWare: undefined,
  apiClient: undefined,
  utils: undefined
}

const errors = {
  ComponentError,
  FailureIncident,
  HardError,
  InvalidConfig,
  InvalidMessageStructureError,
  InvalidMessageContentError,
  SoftError,
  UnknownEventError
}

function resetBPState() {
  bpState.tracer = undefined
  bpState.sqlDbClient = undefined
  bpState.mqClient = undefined
  bpState.cacheClient = undefined
  bpState.routesMiddleWare = undefined
  bpState.apiClient = undefined
  bpState.utils = undefined
}

// ****************************************************************************
// register hooks to print error for uncaught errors
// ****************************************************************************
process.on('uncaughtException', (err) => {
  logger.error('UncaughtException %s', err.stack)
})

// register hooks to print error for uncaught promises
process.on('unhandledRejection', (err) => {
  logger.error('unhandledRejection %s', err.stack)
})

// signals on which the app should shutdown itself
const shutdownSignals = ['SIGINT', 'SIGHUP', 'SIGTERM', 'SIGQUIT']
shutdownSignals.forEach((signal) => {
  process.on(signal, () => { // register signal handlers
    logger.info('Shutdown signal received.')
    process.exit()
  })
})

process.on('exit', () => {
  logger.info('Service shutdown')
})

// ****************************************************************************
// Setup logging and tracing *******************************************
const { setTracer, getTracer } = require('./src/trace/tracer')
const zipkinMiddleware = require('zipkin-instrumentation-express').expressMiddleware

const namespace = cls.getNamespace('default')
function setTransactionIdSupport(app, serviceSetup) {
  logger.info('Doing setup for Transaction ID support. Using Header key %s', serviceSetup.trace.transactionIdHeaderKey)
  if (!serviceSetup.trace.transactionIdHeaderKey) {
    logger.error('Cannot setup tracer. Missing transactionIdHeaderKey')
    throw new InvalidConfig('Missing configuration settings for setup of tracer')
  }
  const tracer = getTracer()
  app.use(zipkinMiddleware({ tracer, serviceName: serviceSetup.microserviceName })) // adding zipkin middleware

  // set transactionid
  app.use((req, res, next) => {
    let tid = req.headers[serviceSetup.trace.transactionIdHeaderKey]
    if (tid === undefined) {
      logger.warn(`${serviceSetup.trace.transactionIdHeaderKey} not provided`)
      tid = uuid.v4()
    }
    res.setHeader(serviceSetup.trace.transactionIdHeaderKey, tid)
    namespace.bindEmitter(req)
    namespace.bindEmitter(res)
    namespace.run(() => {
      namespace.set('transactionid', tid)
      next()
    })
  })
  logger.info('App set to use header key = %s to track transaction Id for logging and tracing', serviceSetup.trace.transactionIdHeaderKey)
  bpState.tracer = tracer
}

// ****************************************************************************
// Setup cache client interface *******************************************
const { startRedis } = require('./src/cache/redis')
const { setRedisKey, getRedisKey, deleteRedisKey } = require('./src/cache/redis')

async function setupRedis() {
  logger.info('Doing setup for Redis Cache Client')
  try {
    await startRedis()
  } catch (err) {
    logger.error('Failed redis connect : %s', err)
    throw err
  }

  bpState.cacheClient = {
    setRedisKey,
    getRedisKey,
    deleteRedisKey
  }
}
// ****************************************************************************
// Setup Message Queue interface *******************************************
const { startMq } = require('./src/mq/init')
const { sendRequest, sendResponse, sendRetryRequest } = require('./src/mq/publish-message')
const { sendIncident } = require('./src/mq/publish-message')

async function setupMQ(mqListeners) {
  logger.info('Doing setup for MQ Client')
  try {
    await startMq(mqListeners)
  } catch (err) {
    logger.error('Failed mq connect : %s', err)
    throw err
  }

  bpState.mqClient = {
    sendRequest,
    sendResponse,
    sendRetryRequest,
    sendIncident
  }
}
// ****************************************************************************
// Setup database interface *******************************************
const { initializeDb } = require('./src/sqldb/db')

async function setupDatabase(serviceSetup) {
  logger.info(`Doing setup for SQL Database for env ${serviceSetup.env}`)
  const dbClient = await initializeDb(serviceSetup.mq.enforceConsecutiveFlow, serviceSetup.db.config[serviceSetup.env], serviceSetup.db.modelsPath)
  bpState.sqlDbClient = dbClient
}
// ****************************************************************************
// Setup interface for middleware *******************************************
const express = require('express')
const swagger = require('swagger-spec-express')
const swaggerUi = require('swagger-ui-express')
const bodyParser = require('body-parser')
const responseTime = require('response-time')
const cookieParser = require('cookie-parser')
const uuid = require('uuid')
const path = require('path')

const { api } = require('./src/middleware/api-framework')
const { routeMiddleware } = require('./src/middleware/route-middleware')
const routes = require('./src/routes/index')

const router = express.Router()
swagger.swaggerize(router)

async function getRoutesMiddleware(app, serviceSetup) {
  logger.info('Bootstrapping middleware..')
  setupSwaggerSpecs(app, serviceSetup)
  setupInterceptors(app, serviceSetup, false)
  await setupRoutes(app, serviceSetup.routes)

  bpState.routesMiddleWare = {
    swagger,
    router
  }
}

function setupRoutes(app, appRoutes) {
  logger.info('Adding application routes for middleware')

  app.use('/', routes)
  if (appRoutes && appRoutes.enabled && appRoutes.routeFolderPath) {
    routeMiddleware(app, router, appRoutes.routeFolderPath)
    logger.info('Done registering routes')
  }
}

function seperateInternalSwaggerSpecs(swaggerSpecs) {
  // initialize internal and external spec
  const internalSpec = { ...swaggerSpecs };
  const externalSpec = { ...swaggerSpecs };

  // delete paths in internal and external specs
  delete internalSpec.paths;
  internalSpec.paths = {};
  delete externalSpec.paths;
  externalSpec.paths = {};

  Object.keys(swaggerSpecs.paths).forEach((path) => {
    if (path) {
      const currentPath = swaggerSpecs.paths[path];

      Object.keys(currentPath).forEach((pathType) => {
        const currentPathType = currentPath[pathType];

        if (currentPathType.tags && currentPathType.tags.includes('internal')) {
          if (!internalSpec.paths[path]) { internalSpec.paths[path] = {} }
          internalSpec.paths[path][pathType] = currentPathType;
        } else {
          if (!externalSpec.paths[path]) { externalSpec.paths[path] = {} }
          externalSpec.paths[path][pathType] = currentPathType;
        }
      })
    }
  })

  return {
    internal: { ...internalSpec },
    external: { ...externalSpec }
  }
}


function generateApiDocs(docPath) {
  logger.info('Generating api docs for your service..')
  swagger.swaggerize(router);
  // compile swagger spec for all routes
  swagger.compile()

  router.get('/api-docs', swaggerUi.serve, swaggerUi.setup(swagger.json())).describe({
    tags: ['internal'],
    responses: {
      200: {
        description: 'this is api docs'
      }
    }
  })

  // generate swagger spec
  const swaggerSpecs = swagger.json()
  // seperate swagger spec for internal and external routes
  const specs = seperateInternalSwaggerSpecs({ ...swaggerSpecs });

  // generate swagger spec for the external routes
  let swaggerConverter = new Swagger2Postman()
  const externalPostmanSpecs = swaggerConverter.convert(specs.external);

  // generate swagger specs for internal routes
  swaggerConverter = new Swagger2Postman()
  const internalPostmanSpecs = swaggerConverter.convert(specs.internal);

  // initialize spec path at root if config is not defined in the serive for spec generation
  const postmanPath = docPath.postmanSpecPath ? docPath.postmanSpecPath : process.cwd();
  const swaggerPath = docPath.swaggerSpecPath ? docPath.swaggerSpecPath : process.cwd();

  // generate internal swagger specs
  fs.writeFileSync(`${swaggerPath}internal-swagger-spec.json`, JSON.stringify(specs.internal), { flag: 'w' })
  logger.info('Swagger Specs Saved for Internal API!')

  // generate external swagger specs
  fs.writeFileSync(`${swaggerPath}swagger-spec.json`, JSON.stringify(specs.external), { flag: 'w' })
  logger.info('Swagger Specs Saved for External API!')

  // generate internal postman specs
  fs.writeFileSync(`${postmanPath}internal-api-collection.json`, JSON.stringify(internalPostmanSpecs), { flag: 'w' })
  logger.info('Postman Specs Saved for Internal API!')

  // generate external swagger specs
  fs.writeFileSync(`${postmanPath}api-collection.json`, JSON.stringify(externalPostmanSpecs), { flag: 'w' })
  logger.info('Postman Specs Saved for External API!')

  logger.info('API Docs generated for the service')

  return true;
}

function setupInterceptors(app, serviceSetup, hasFavicon) {
  // view engine setup
  app.set('views', path.join(__dirname, 'src/views'))
  app.set('view engine', 'jade')

  if (hasFavicon) {
    // TODO : where is this favIcon function ?
    // app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  }

  app.use(expressSanitizer())
  app.use(appLogger('combined', {
    stream: logger.stream
    // skip (req) { return req.url === '/health' }
  }))
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(cookieParser())
  app.use(express.static(path.join(__dirname, 'public')))
  app.use(responseTime())

  app.use((req, res, next) => {
    res.append('x-service-name', serviceSetup.microserviceName)
    res.append('x-service-version', serviceSetup.microserviceVersion)
    next()
  })

  app.use((req, res, next) => {
    req.start = Date.now()
    next()
  })
}

function setupSwaggerSpecs(app, serviceSetup) {
  logger.info('Doing setup for Swagger specs')
  // swagger seup
  const options = {
    title: serviceSetup.microserviceName,
    version: serviceSetup.microserviceVersion,
    description: packageJson.description,
    host: '',
    schemes: ['http', 'https'],
    securityDefinitions: {
      apiKeyHeader: {
        type: 'apiKey',
        name: 'Ocp-Apim-Subscription-Key',
        in: 'header'
      },
      apiKeyQuery: {
        type: 'apiKey',
        name: 'subscription-key',
        in: 'query'
      }
    },
    security: [
      {
        apiKeyHeader: []
      },
      {
        apiKeyQuery: []
      }
    ]
  }
  logger.info('Preparing swagger specs for %j ', options.title)
  swagger.reset()
  swagger.initialise(app, options)
}
// ****************************************************************************

// Setup helper methods *******************************************
const { setupServiceConfig } = require('./src/setup')
const createRequest = require('./src/utils/request-utils')

const serviceBootstrapper = async function (serviceSetup, mqListeners, app) {
  console.log('Bootstraping Service %s, version %s', serviceSetup.microserviceName, serviceSetup.microserviceVersion)
  resetBPState()
  const apifiedApp = {}
  apifiedApp.app = app
  try {
    setupServiceConfig(serviceSetup, mqListeners)

    if (serviceSetup.trace.enabled) {
      setTracer()
      await setTransactionIdSupport(app, serviceSetup)
      eventBus.emit('TRACER_READY', apifiedApp.trace)
    }
    if (serviceSetup.cache.enabled) {
      await setupRedis(serviceSetup)
      eventBus.emit('CACHE_READY', apifiedApp.cache)
    }
    if (serviceSetup.db.enabled) {
      await setupDatabase(serviceSetup)
      eventBus.emit('SQLDB_READY', apifiedApp.sqlDb)
    }
    if (serviceSetup.mq.enabled) {
      await setupMQ(mqListeners)
      eventBus.emit('MQ_READY', apifiedApp.mq)
    }
    if (serviceSetup.routes.enabled) {
      await getRoutesMiddleware(app, serviceSetup)
      eventBus.emit('MIDDLEWARE_READY', apifiedApp.routes)
    }
    if (serviceSetup.api.enabled) {
      bpState.apiClient = api
      eventBus.emit('API_READY', apifiedApp.routes)
    }

    bpState.utils = {
      createRequest,
      generateApiDocs
    }

    eventBus.emit('SERVICE_BOOTSTRAP_COMPLETE', apifiedApp)
    console.log('Your service %s-%s is now ready to serve', serviceSetup.microserviceName, serviceSetup.microserviceVersion)
    console.log('****************************************************************')
    return app
  } catch (err) {
    console.error('Failed to bootstrap your service')
    console.error(err)
    throw err
  }
}

function getTracerClient() {
  if (bpState.tracer) {
    return bpState.tracer
  }
  throw new ComponentError('Tracer not ready yet', null)
}

function getCacheClient() {
  if (bpState.cacheClient) {
    return bpState.cacheClient
  }
  throw new ComponentError('Cache not ready yet', null)
}

function getSqlDBClient() {
  if (bpState.sqlDbClient) {
    return bpState.sqlDbClient
  }
  throw new ComponentError('SQL DB not ready yet', null)
}

function getMqClient() {
  if (bpState.mqClient) {
    return bpState.mqClient
  }
  throw new ComponentError('MQ not ready yet', null)
}

function getMiddleware() {
  if (bpState.routesMiddleWare) {
    return bpState.routesMiddleWare
  }
  throw new ComponentError('Middleware not ready yet', null)
}

function getApiClient() {
  if (bpState.apiClient) {
    return bpState.apiClient
  }
  throw new ComponentError('API not ready yet', null)
}

function getUtils() {
  if (bpState.utils) {
    return bpState.utils
  }
  throw new ComponentError('Utils not ready yet', null)
}

console.log('Node JS BP module is ready to load. Somebody need to call a startApp function that I provide.')

module.exports = {
  getTracerClient,
  getCacheClient,
  getSqlDBClient,
  getMqClient,
  getMiddleware,
  getApiClient,
  getUtils,
  logger,
  errors,
  serviceBootstrapper,
  router,
  swagger,
  eventBus
}
